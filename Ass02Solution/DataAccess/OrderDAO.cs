﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class OrderDAO
    {
        private static OrderDAO instance = null;
        private static readonly object intanceLock = new object();
        public static OrderDAO Instance
        {
            get
            {
                lock (intanceLock)
                {
                    if (instance == null)
                    {
                        instance = new OrderDAO();
                    }
                    return instance;
                }
            }
        }
        public void AddOrder(Order order)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Orders.Add(order);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Order> GetAllOrder()
        {
            List<Order> orders;
            try
            {
                using MyStockContext stock = new MyStockContext();
                orders = stock.Orders.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return orders;
        }
        public void UpdateOrder(Order order)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Entry<Order>(order).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteOrder(int orderid)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                var o = stock.Orders.SingleOrDefault(O => O.OrderId == orderid);
                List<OrderDetail> od = stock.OrderDetails.Where(o => o.OrderId == orderid).ToList();
                stock.OrderDetails.RemoveRange(od);
                stock.Orders.Remove(o);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Order GetOrder(int orderid)
        {
            Order order;
            try
            {
                using MyStockContext stock = new MyStockContext();
                order = stock.Orders.SingleOrDefault(O => O.OrderId == orderid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return order;
        }
        public List<Order> SearchOrder(string filter, string key)
        {
            List<Order> orders = new List<Order>();
            if (filter.Equals("OrderID"))
            {
                try
                {
                    int orderid = int.Parse(key);
                    using MyStockContext stock = new MyStockContext();
                    var o = stock.Orders.SingleOrDefault(O => O.OrderId == orderid);
                    orders.Add(o);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                int memberid = int.Parse(key);
                using MyStockContext stock = new MyStockContext();
                orders = stock.Orders.Where(m => m.MemberId.Equals(memberid)).ToList();
            }
            return orders;
        }

        public int GetLastOrderId()
        {
            try
            {
                using (MyStockContext stock = new MyStockContext())
                {
                    int lastOrderId = stock.Orders
                        .OrderByDescending(o => o.OrderId)
                        .Select(o => o.OrderId)
                        .FirstOrDefault();

                    return lastOrderId;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
