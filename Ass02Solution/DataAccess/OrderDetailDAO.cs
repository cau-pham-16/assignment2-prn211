﻿using BusinessObject.BusinessObject;
using DataAccess.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class OrderDetailDAO
    {
        private static OrderDetailDAO instance = null;
        private static readonly object intanceLock = new object();
        public static OrderDetailDAO Instance
        {
            get
            {
                lock (intanceLock)
                {
                    if (instance == null)
                    {
                        instance = new OrderDetailDAO();
                    }
                    return instance;
                }
            }
        }
        public void AddOrderDetail(List<OrderDetail> orderDetails)
        {
            try
            {
                using (MyStockContext stock = new MyStockContext())
                {
                    foreach (var orderDetail in orderDetails)
                    {
                        stock.Database.ExecuteSqlRaw("INSERT INTO OrderDetail (OrderId,ProductId, Quantity, UnitPrice, Discount) VALUES ({0}, {1}, {2}, {3},{4})",orderDetail.OrderId, orderDetail.ProductId, orderDetail.Quantity, orderDetail.UnitPrice, orderDetail.Discount);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        public List<OrderDetail> GetAllOrderDetail()
        {
            List<OrderDetail> orderdetails;
            try
            {
                using MyStockContext stock = new MyStockContext();
                orderdetails = stock.OrderDetails.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return orderdetails;
        }
        public void UpdateOrderDetail(OrderDetail orderDetail)
        {
            try
            {
                using (MyStockContext stock = new MyStockContext())
                {
                    stock.Database.ExecuteSqlRaw("UPDATE OrderDetail SET Discount = {0} WHERE OrderId = {1} AND ProductId = {2}", orderDetail.Discount, orderDetail.OrderId, orderDetail.ProductId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteOrderDetail(OrderDetail orderDetail)
        {
            try
            {
                using (MyStockContext stock = new MyStockContext())
                {
                    stock.Database.ExecuteSqlRaw("DELETE FROM OrderDetail WHERE OrderId = {0} AND ProductId = {1}", orderDetail.OrderId, orderDetail.ProductId);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<OrderDetail> GetOrderDetail(int orderid)
        {
            List<OrderDetail> orderDetails = new List<OrderDetail>();
            try
            {
                using MyStockContext stock = new MyStockContext();
                orderDetails = stock.OrderDetails.Where(o => o.OrderId == orderid).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return orderDetails;
        }
    }
}
