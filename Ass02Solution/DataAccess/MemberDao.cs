﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class MemberDAO
    {
        private static MemberDAO instance = null;
        private static readonly object intanceLock = new object();
        public static MemberDAO Instance
        {
            get
            {
                lock (intanceLock)
                {
                    if (instance == null)
                    {
                        instance = new MemberDAO();
                    }
                    return instance;
                }
            }
        }
        public void AddMember(Member member)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Members.Add(member);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Member> GetAllMember()
        {
            List<Member> members;
            try
            {
                using MyStockContext stock = new MyStockContext();
                members = stock.Members.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return members;
        }
        public void UpdateMember(Member member)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Entry<Member>(member).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteMember(int memberid)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                var o = stock.Members.SingleOrDefault(O => O.MemberId == memberid);
                stock.Members.Remove(o);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Member GetMember(int memberid)
        {
            Member member;
            try
            {
                using MyStockContext stock = new MyStockContext();
                member = stock.Members.SingleOrDefault(O => O.MemberId == memberid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return member;
        }
        public Member GetMemberLogin(string email, string password)
        {
            Member member;
            try
            {
                using MyStockContext stock = new MyStockContext();
                member = stock.Members.FirstOrDefault(p => p.Email == email && p.Password == password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return member;
        }
        public List<Member> SearchMember(string filter, string key)
        {
            List<Member> members = new List<Member>();
            if (filter.Equals("ID"))
            {
                try
                {
                    int memberId = int.Parse(key);
                    using MyStockContext stock = new MyStockContext();
                    var member = stock.Members.SingleOrDefault(O => O.MemberId == memberId);
                    members.Add(member);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                using MyStockContext stock = new MyStockContext();
                members = stock.Members.Where(m => m.CompanyName.Contains(key)).ToList();
            }
            return members;
        }
    }
}
