﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class ProductRepository : IProductRepository
    {
        public void DeleteProduct(int productid) => ProductDAO.Instance.DeleteProduct(productid);

        public IEnumerable<Product> GetAllProduct() => ProductDAO.Instance.GetAllProduct();
        public Product GetProduct(int productid) =>ProductDAO.Instance.GetProduct(productid);
        public void InsertProduct(Product product) => ProductDAO.Instance.AddProduct(product);
        public void UpdateProduct(Product product) => ProductDAO.Instance.UpdateProduct(product);
    }
}
