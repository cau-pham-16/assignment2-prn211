﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IMemberRepositorycs
    {
        IEnumerable<Member> GetAllMember();
        Member GetMember(int carID);
        void InsertMember(Member member);
        void Update(Member member);
        void DeleteMember(int memberid);
    }
}
