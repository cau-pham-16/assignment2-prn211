﻿using BusinessObject.BusinessObject;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class MemberRepository : IMemberRepositorycs
    {
        public void DeleteMember(int memberid) => MemberDAO.Instance.DeleteMember(memberid);

        public IEnumerable<Member> GetAllMember() => MemberDAO.Instance.GetAllMember();

        public Member GetMember(int carID) => MemberDAO.Instance.GetMember(carID);
        public void InsertMember(Member member) =>MemberDAO.Instance.AddMember(member);
        public void Update(Member member) => MemberDAO.Instance.UpdateMember(member);
        public Member GetMemberLogin(string email, string pass) => MemberDAO.Instance.GetMemberLogin(email, pass);  
        public List<Member> SearchMember(string filter, string key) => MemberDAO.Instance.SearchMember(filter, key);
    }
}
