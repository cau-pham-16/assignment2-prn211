﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetAllProduct();
        Product GetProduct(int productid);
        void InsertProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(int productid);
    }
}
