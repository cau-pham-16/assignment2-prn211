﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public class OrderRepository : IOrderRepository
    {
        public void DeleteOrder(int orderid) => OrderDAO.Instance.DeleteOrder(orderid);

        public IEnumerable<Order> GetAllOrder() => OrderDAO.Instance.GetAllOrder();
        public Order GetOrder(int orderid) => OrderDAO.Instance.GetOrder(orderid);
        public void InsertOrder(Order order) => OrderDAO.Instance.AddOrder(order);

        public void UpdateOrder(Order order) => OrderDAO.Instance.UpdateOrder(order);
    }
}
