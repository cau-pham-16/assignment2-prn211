﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace DataAccess.Repository
{
    public class OrderDetailrepository : IOrderDetailRepository
    {
        public void DeleteOrderDetail(OrderDetail od) => OrderDetailDAO.Instance.DeleteOrderDetail(od);        
        public IEnumerable<OrderDetail> GetAllOrderDetail() => OrderDetailDAO.Instance.GetAllOrderDetail();       
        public List<OrderDetail> GetOredrDetail(int orderid) => OrderDetailDAO.Instance.GetOrderDetail(orderid);
        public void InsertOrderDetail(List<OrderDetail> od) => OrderDetailDAO.Instance.AddOrderDetail(od);      
        public void UpdateOrderDetail(OrderDetail od) => OrderDetailDAO.Instance.UpdateOrderDetail(od);
        
    }
}
