﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IOrderDetailRepository
    {
        IEnumerable<OrderDetail> GetAllOrderDetail();
        List<OrderDetail> GetOredrDetail(int orderid);
        void InsertOrderDetail(List<OrderDetail> od);
        void UpdateOrderDetail(OrderDetail od);
        void DeleteOrderDetail(OrderDetail od);
    }
}
