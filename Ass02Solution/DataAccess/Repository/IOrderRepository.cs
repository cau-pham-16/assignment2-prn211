﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repository
{
    public interface IOrderRepository
    {
        IEnumerable<Order> GetAllOrder();
        Order GetOrder(int orderid);
        void InsertOrder(Order order);
        void UpdateOrder(Order order);
        void DeleteOrder(int orderid);
    }
}
