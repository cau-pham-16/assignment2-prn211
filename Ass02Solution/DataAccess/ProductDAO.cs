﻿using BusinessObject.BusinessObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class ProductDAO
    {
        private static ProductDAO instance = null;
        private static readonly object intanceLock = new object();
        public static ProductDAO Instance
        {
            get
            {
                lock (intanceLock)
                {
                    if (instance == null)
                    {
                        instance = new ProductDAO();
                    }
                    return instance;
                }
            }
        }
        public void AddProduct(Product product)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Products.Add(product);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public List<Product> GetAllProduct()
        {
            List<Product> products;
            try
            {
                using MyStockContext stock = new MyStockContext();
                products = stock.Products.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return products;
        }
        public void UpdateProduct(Product product)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                stock.Entry<Product>(product).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void DeleteProduct(int productid)
        {
            try
            {
                using MyStockContext stock = new MyStockContext();
                var o = stock.Products.SingleOrDefault(O => O.ProductId == productid);
                stock.Products.Remove(o);
                stock.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Product GetProduct(int productid)
        {
            Product product;
            try
            {
                using MyStockContext stock = new MyStockContext();
                product = stock.Products.SingleOrDefault(O => O.ProductId == productid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return product;
        }
        public List<Product> SearchProduct(string filter, string key)
        {
            List<Product> products = new List<Product>();
            if (filter.Equals("ID"))
            {
                try
                {
                    int productid = int.Parse(key);
                    using MyStockContext stock = new MyStockContext();
                    var p = stock.Products.SingleOrDefault(O => O.ProductId == productid);
                    products.Add(p);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                using MyStockContext stock = new MyStockContext();
                products = stock.Products.Where(m => m.ProductName.Contains(key)).ToList();
            }
            return products;
        }
    }
}
