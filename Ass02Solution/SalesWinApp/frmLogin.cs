﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessObject.BusinessObject;
using DataAccess.Repository;
namespace SalesWinApp
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }
        public IMemberRepositorycs memberRespository = new MemberRepository();
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string jsonFilePath = "./appsetting.json";

            // Đọc tệp JSON vào một chuỗi
            string jsonString = File.ReadAllText(jsonFilePath);

            // Phân tích chuỗi JSON thành đối tượng
            var jsonObject = JsonDocument.Parse(jsonString).RootElement;

            // Truy cập vào các thuộc tính trong đối tượng JSON
            string email = jsonObject.GetProperty("Email").GetString();
            string password = jsonObject.GetProperty("Pass").GetString();
            MemberRepository memberRepository = new MemberRepository();
            if (email.Equals(txtEmail.Text) && password.Equals(txtPass.Text))
            {
                frmMain frmmain = new frmMain();
                this.Hide();
                frmmain.ShowDialog();
                this.Close();

            }
            else if (memberRepository.GetMemberLogin(txtEmail.Text, txtPass.Text) != null)
            {
                Member member = memberRepository.GetMemberLogin(txtEmail.Text, txtPass.Text);
                frmCustomer frm = new frmCustomer
                {
                    member = member,
                };
                this.Hide();
                frm.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Login not successful");
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
