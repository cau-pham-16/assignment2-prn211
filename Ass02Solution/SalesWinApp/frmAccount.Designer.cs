﻿namespace SalesWinApp
{
    partial class frmAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            btnSave = new Button();
            btnCancel = new Button();
            lbMemID = new Label();
            lbEmail = new Label();
            lbPass = new Label();
            lbCompany = new Label();
            lbCity = new Label();
            lbCountry = new Label();
            txtMemID = new TextBox();
            txtEmail = new TextBox();
            txtPass = new TextBox();
            txtCompany = new TextBox();
            txtCity = new TextBox();
            txtCountry = new TextBox();
            SuspendLayout();
            // 
            // btnSave
            // 
            btnSave.DialogResult = DialogResult.OK;
            btnSave.Location = new Point(279, 278);
            btnSave.Margin = new Padding(3, 2, 3, 2);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(82, 22);
            btnSave.TabIndex = 0;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = DialogResult.Cancel;
            btnCancel.Location = new Point(90, 278);
            btnCancel.Margin = new Padding(3, 2, 3, 2);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(82, 22);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // lbMemID
            // 
            lbMemID.AutoSize = true;
            lbMemID.Location = new Point(60, 27);
            lbMemID.Name = "lbMemID";
            lbMemID.Size = new Size(63, 15);
            lbMemID.TabIndex = 2;
            lbMemID.Text = "MemberID";
            // 
            // lbEmail
            // 
            lbEmail.AutoSize = true;
            lbEmail.Location = new Point(60, 75);
            lbEmail.Name = "lbEmail";
            lbEmail.Size = new Size(36, 15);
            lbEmail.TabIndex = 3;
            lbEmail.Text = "Email";
            // 
            // lbPass
            // 
            lbPass.AutoSize = true;
            lbPass.Location = new Point(60, 116);
            lbPass.Name = "lbPass";
            lbPass.Size = new Size(57, 15);
            lbPass.TabIndex = 4;
            lbPass.Text = "Password";
            // 
            // lbCompany
            // 
            lbCompany.AutoSize = true;
            lbCompany.Location = new Point(60, 154);
            lbCompany.Name = "lbCompany";
            lbCompany.Size = new Size(91, 15);
            lbCompany.TabIndex = 5;
            lbCompany.Text = "CompanyName";
            // 
            // lbCity
            // 
            lbCity.AutoSize = true;
            lbCity.Location = new Point(60, 194);
            lbCity.Name = "lbCity";
            lbCity.Size = new Size(28, 15);
            lbCity.TabIndex = 6;
            lbCity.Text = "City";
            // 
            // lbCountry
            // 
            lbCountry.AutoSize = true;
            lbCountry.Location = new Point(60, 230);
            lbCountry.Name = "lbCountry";
            lbCountry.Size = new Size(50, 15);
            lbCountry.TabIndex = 7;
            lbCountry.Text = "Country";
            // 
            // txtMemID
            // 
            txtMemID.Location = new Point(175, 27);
            txtMemID.Margin = new Padding(3, 2, 3, 2);
            txtMemID.Name = "txtMemID";
            txtMemID.Size = new Size(186, 23);
            txtMemID.TabIndex = 8;
            // 
            // txtEmail
            // 
            txtEmail.Location = new Point(175, 73);
            txtEmail.Margin = new Padding(3, 2, 3, 2);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new Size(186, 23);
            txtEmail.TabIndex = 9;
            // 
            // txtPass
            // 
            txtPass.Location = new Point(175, 113);
            txtPass.Margin = new Padding(3, 2, 3, 2);
            txtPass.Name = "txtPass";
            txtPass.Size = new Size(186, 23);
            txtPass.TabIndex = 10;
            // 
            // txtCompany
            // 
            txtCompany.Location = new Point(175, 152);
            txtCompany.Margin = new Padding(3, 2, 3, 2);
            txtCompany.Name = "txtCompany";
            txtCompany.Size = new Size(186, 23);
            txtCompany.TabIndex = 11;
            // 
            // txtCity
            // 
            txtCity.Location = new Point(175, 189);
            txtCity.Margin = new Padding(3, 2, 3, 2);
            txtCity.Name = "txtCity";
            txtCity.Size = new Size(186, 23);
            txtCity.TabIndex = 12;
            // 
            // txtCountry
            // 
            txtCountry.Location = new Point(175, 224);
            txtCountry.Margin = new Padding(3, 2, 3, 2);
            txtCountry.Name = "txtCountry";
            txtCountry.Size = new Size(186, 23);
            txtCountry.TabIndex = 13;
            // 
            // frmAccount
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(452, 340);
            Controls.Add(txtCountry);
            Controls.Add(txtCity);
            Controls.Add(txtCompany);
            Controls.Add(txtPass);
            Controls.Add(txtEmail);
            Controls.Add(txtMemID);
            Controls.Add(lbCountry);
            Controls.Add(lbCity);
            Controls.Add(lbCompany);
            Controls.Add(lbPass);
            Controls.Add(lbEmail);
            Controls.Add(lbMemID);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmAccount";
            Text = "frmMember";
            Load += frmAccount_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        private Button btnSave;
        private Button btnCancel;
        private Label lbMemID;
        private Label lbEmail;
        private Label lbPass;
        private Label lbCompany;
        private Label lbCity;
        private Label lbCountry;
        private TextBox txtMemID;
        private TextBox txtEmail;
        private TextBox txtPass;
        private TextBox txtCompany;
        private TextBox txtCity;
        private TextBox txtCountry;
    }
}