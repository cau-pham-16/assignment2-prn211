﻿using BusinessObject.BusinessObject;
using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinApp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        IMemberRepositorycs memberRepository = new MemberRepository();
        BindingSource source;
        private void lbCompany_Click(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            List<Member> members = new List<Member>();
            btnDelete.Enabled = false;
            try
            {
                members = (List<Member>)memberRepository.GetAllMember();
                source = new BindingSource();
                source.DataSource = members;
                txtMemID.DataBindings.Clear();
                txtEmail.DataBindings.Clear();
                txtPass.DataBindings.Clear();
                txtCompany.DataBindings.Clear();
                txtCity.DataBindings.Clear();
                txtCountry.DataBindings.Clear();

                txtMemID.DataBindings.Add("Text", source, "MemberID");
                txtEmail.DataBindings.Add("Text", source, "Email");
                txtPass.DataBindings.Add("Text", source, "Password");
                txtCompany.DataBindings.Add("Text", source, "CompanyName");
                txtCity.DataBindings.Add("Text", source, "City");
                txtCountry.DataBindings.Add("Text", source, "Country");
                dgvMember.DataSource = null;
                dgvMember.DataSource = source;
                if (members.Count() == 0)
                {
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Member List");
            }
        }

        private void dgvMember_DoubleClick(object sender, EventArgs e)
        {
            frmMember fm = new frmMember
            {
                Text = "Edit Member",
                InsertOrUpdate = true,
                MemberInfor = GetMember(),
                memberRepository = memberRepository
            };
            if (fm.ShowDialog() == DialogResult.OK)
            {
                fm.Close();
                fm.Hide();
                frmMain_Load(sender, e);
            }
        }
        private Member GetMember()
        {
            Member member = new Member();
            try
            {
                member = new Member
                {
                    MemberId = int.Parse(txtMemID.Text),
                    Email = txtEmail.Text,
                    Password = txtPass.Text,
                    CompanyName = txtCompany.Text,
                    City = txtCity.Text,
                    Country = txtCountry.Text
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Member");
            }
            return member;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmMember fm = new frmMember
            {
                Text = "Create Member",
                InsertOrUpdate = false,
                MemberInfor = GetMember(),
                memberRepository = memberRepository
            };
            if (fm.ShowDialog() == DialogResult.OK)
            {
                fm.Close();
                fm.Hide();
                frmMain_Load(sender, e);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Member member = GetMember();
            memberRepository.DeleteMember(member.MemberId);
            frmMain_Load(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String filter = cbxSearch.Text;
            String key = txtSearch.Text;
            List<Member> members = new List<Member>();
            MemberDAO md = new MemberDAO();
            members = md.SearchMember(filter, key);
            source = new BindingSource();
            source.DataSource = members;
            txtMemID.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            txtPass.DataBindings.Clear();
            txtCompany.DataBindings.Clear();
            txtCity.DataBindings.Clear();
            txtCountry.DataBindings.Clear();

            txtMemID.DataBindings.Add("Text", source, "MemberID");
            txtEmail.DataBindings.Add("Text", source, "Email");
            txtPass.DataBindings.Add("Text", source, "Password");
            txtCompany.DataBindings.Add("Text", source, "CompanyName");
            txtCity.DataBindings.Add("Text", source, "City");
            txtCountry.DataBindings.Add("Text", source, "Country");
            dgvMember.DataSource = null;
            dgvMember.DataSource = source;
        }

        private void btnProductManager_Click(object sender, EventArgs e)
        {
            frmManagerProduct fm = new frmManagerProduct();
            this.Hide();
            fm.ShowDialog();
            this.Close();

        }

        private void btnOrderManager_Click(object sender, EventArgs e)
        {
            frmManagerOrder fm = new frmManagerOrder();
            this.Hide();
            fm.ShowDialog();
            this.Close();
        }
    }
}
