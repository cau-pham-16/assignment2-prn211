﻿namespace SalesWinApp
{
    partial class frmOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            btnSave = new Button();
            btnCancel = new Button();
            lbOrderID = new Label();
            lbMemberID = new Label();
            lbOrderDate = new Label();
            lbRequiredDate = new Label();
            lbShippedDate = new Label();
            lbFreight = new Label();
            txtOrderID = new TextBox();
            txtMemberID = new TextBox();
            txtOrderDate = new TextBox();
            txtRequiredDate = new TextBox();
            txtShippedDate = new TextBox();
            txtFreight = new TextBox();
            SuspendLayout();
            // 
            // btnSave
            // 
            btnSave.DialogResult = DialogResult.OK;
            btnSave.Location = new Point(279, 278);
            btnSave.Margin = new Padding(3, 2, 3, 2);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(82, 22);
            btnSave.TabIndex = 0;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = DialogResult.Cancel;
            btnCancel.Location = new Point(90, 278);
            btnCancel.Margin = new Padding(3, 2, 3, 2);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(82, 22);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // lbOrderID
            // 
            lbOrderID.AutoSize = true;
            lbOrderID.Location = new Point(60, 27);
            lbOrderID.Name = "lbOrderID";
            lbOrderID.Size = new Size(48, 15);
            lbOrderID.TabIndex = 2;
            lbOrderID.Text = "OrderID";
            // 
            // lbMemberID
            // 
            lbMemberID.AutoSize = true;
            lbMemberID.Location = new Point(60, 75);
            lbMemberID.Name = "lbMemberID";
            lbMemberID.Size = new Size(63, 15);
            lbMemberID.TabIndex = 3;
            lbMemberID.Text = "MemberID";
            // 
            // lbOrderDate
            // 
            lbOrderDate.AutoSize = true;
            lbOrderDate.Location = new Point(60, 116);
            lbOrderDate.Name = "lbOrderDate";
            lbOrderDate.Size = new Size(61, 15);
            lbOrderDate.TabIndex = 4;
            lbOrderDate.Text = "OrderDate";
            // 
            // lbRequiredDate
            // 
            lbRequiredDate.AutoSize = true;
            lbRequiredDate.Location = new Point(60, 154);
            lbRequiredDate.Name = "lbRequiredDate";
            lbRequiredDate.Size = new Size(78, 15);
            lbRequiredDate.TabIndex = 5;
            lbRequiredDate.Text = "RequiredDate";
            // 
            // lbShippedDate
            // 
            lbShippedDate.AutoSize = true;
            lbShippedDate.Location = new Point(60, 194);
            lbShippedDate.Name = "lbShippedDate";
            lbShippedDate.Size = new Size(74, 15);
            lbShippedDate.TabIndex = 6;
            lbShippedDate.Text = "ShippedDate";
            // 
            // lbFreight
            // 
            lbFreight.AutoSize = true;
            lbFreight.Location = new Point(60, 230);
            lbFreight.Name = "lbFreight";
            lbFreight.Size = new Size(44, 15);
            lbFreight.TabIndex = 7;
            lbFreight.Text = "Freight";
            // 
            // txtOrderID
            // 
            txtOrderID.Location = new Point(175, 28);
            txtOrderID.Margin = new Padding(3, 2, 3, 2);
            txtOrderID.Name = "txtOrderID";
            txtOrderID.Size = new Size(186, 23);
            txtOrderID.TabIndex = 8;
            // 
            // txtMemberID
            // 
            txtMemberID.Location = new Point(175, 73);
            txtMemberID.Margin = new Padding(3, 2, 3, 2);
            txtMemberID.Name = "txtMemberID";
            txtMemberID.Size = new Size(186, 23);
            txtMemberID.TabIndex = 9;
            // 
            // txtOrderDate
            // 
            txtOrderDate.Location = new Point(175, 113);
            txtOrderDate.Margin = new Padding(3, 2, 3, 2);
            txtOrderDate.Name = "txtOrderDate";
            txtOrderDate.Size = new Size(186, 23);
            txtOrderDate.TabIndex = 10;
            // 
            // txtRequiredDate
            // 
            txtRequiredDate.Location = new Point(175, 152);
            txtRequiredDate.Margin = new Padding(3, 2, 3, 2);
            txtRequiredDate.Name = "txtRequiredDate";
            txtRequiredDate.Size = new Size(186, 23);
            txtRequiredDate.TabIndex = 11;
            // 
            // txtShippedDate
            // 
            txtShippedDate.Location = new Point(175, 189);
            txtShippedDate.Margin = new Padding(3, 2, 3, 2);
            txtShippedDate.Name = "txtShippedDate";
            txtShippedDate.Size = new Size(186, 23);
            txtShippedDate.TabIndex = 12;
            // 
            // txtFreight
            // 
            txtFreight.Location = new Point(175, 224);
            txtFreight.Margin = new Padding(3, 2, 3, 2);
            txtFreight.Name = "txtFreight";
            txtFreight.Size = new Size(186, 23);
            txtFreight.TabIndex = 13;
            // 
            // frmOrder
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(452, 340);
            Controls.Add(txtFreight);
            Controls.Add(txtShippedDate);
            Controls.Add(txtRequiredDate);
            Controls.Add(txtOrderDate);
            Controls.Add(txtMemberID);
            Controls.Add(txtOrderID);
            Controls.Add(lbFreight);
            Controls.Add(lbShippedDate);
            Controls.Add(lbRequiredDate);
            Controls.Add(lbOrderDate);
            Controls.Add(lbMemberID);
            Controls.Add(lbOrderID);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmOrder";
            Text = "frmOrder";
            Load += frmOrder_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        private Button btnSave;
        private Button btnCancel;
        private Label lbOrderID;
        private Label lbMemberID;
        private Label lbOrderDate;
        private Label lbRequiredDate;
        private Label lbShippedDate;
        private Label lbFreight;
        private TextBox txtOrderID;
        private TextBox txtMemberID;
        private TextBox txtOrderDate;
        private TextBox txtRequiredDate;
        private TextBox txtShippedDate;
        private TextBox txtFreight;
    }
}