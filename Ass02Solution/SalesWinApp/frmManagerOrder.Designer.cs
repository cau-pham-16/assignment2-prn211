﻿namespace SalesWinApp
{
    partial class frmManagerOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnProductManager = new Button();
            btnOrderManager = new Button();
            cbxSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            lbOrderID = new Label();
            lbOrderDate = new Label();
            lbMemberID = new Label();
            lbShippedDate = new Label();
            lbRequiredDate = new Label();
            dgvOrder = new DataGridView();
            btnDelete = new Button();
            btnEdit = new Button();
            btnClose = new Button();
            txtShippedDate = new TextBox();
            txtRequiredDate = new TextBox();
            txtOrderDate = new TextBox();
            txtMemberID = new TextBox();
            txtOrderID = new TextBox();
            lbFreight = new Label();
            txtFreight = new TextBox();
            ((System.ComponentModel.ISupportInitialize)dgvOrder).BeginInit();
            SuspendLayout();
            // 
            // btnProductManager
            // 
            btnProductManager.Location = new Point(446, 9);
            btnProductManager.Margin = new Padding(3, 2, 3, 2);
            btnProductManager.Name = "btnProductManager";
            btnProductManager.Size = new Size(117, 26);
            btnProductManager.TabIndex = 0;
            btnProductManager.Text = "Product Manager";
            btnProductManager.UseVisualStyleBackColor = true;
            btnProductManager.Click += btnProductManager_Click;
            // 
            // btnOrderManager
            // 
            btnOrderManager.Location = new Point(579, 9);
            btnOrderManager.Margin = new Padding(3, 2, 3, 2);
            btnOrderManager.Name = "btnOrderManager";
            btnOrderManager.Size = new Size(110, 26);
            btnOrderManager.TabIndex = 1;
            btnOrderManager.Text = "Order Manager";
            btnOrderManager.UseVisualStyleBackColor = true;
            btnOrderManager.Click += btnOrderManager_Click;
            // 
            // cbxSearch
            // 
            cbxSearch.FormattingEnabled = true;
            cbxSearch.Items.AddRange(new object[] { "OrderID", "MemberID" });
            cbxSearch.Location = new Point(10, 14);
            cbxSearch.Margin = new Padding(3, 2, 3, 2);
            cbxSearch.Name = "cbxSearch";
            cbxSearch.Size = new Size(70, 23);
            cbxSearch.TabIndex = 2;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(86, 16);
            txtSearch.Margin = new Padding(3, 2, 3, 2);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(163, 23);
            txtSearch.TabIndex = 3;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(250, 14);
            btnSearch.Margin = new Padding(3, 2, 3, 2);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(82, 22);
            btnSearch.TabIndex = 4;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // lbOrderID
            // 
            lbOrderID.AutoSize = true;
            lbOrderID.Location = new Point(3, 81);
            lbOrderID.Name = "lbOrderID";
            lbOrderID.Size = new Size(48, 15);
            lbOrderID.TabIndex = 5;
            lbOrderID.Text = "OrderID";
            // 
            // lbOrderDate
            // 
            lbOrderDate.AutoSize = true;
            lbOrderDate.Location = new Point(3, 147);
            lbOrderDate.Name = "lbOrderDate";
            lbOrderDate.Size = new Size(61, 15);
            lbOrderDate.TabIndex = 6;
            lbOrderDate.Text = "OrderDate";
            // 
            // lbMemberID
            // 
            lbMemberID.AutoSize = true;
            lbMemberID.Location = new Point(3, 114);
            lbMemberID.Name = "lbMemberID";
            lbMemberID.Size = new Size(63, 15);
            lbMemberID.TabIndex = 7;
            lbMemberID.Text = "MemberID";
            // 
            // lbShippedDate
            // 
            lbShippedDate.AutoSize = true;
            lbShippedDate.Location = new Point(326, 113);
            lbShippedDate.Name = "lbShippedDate";
            lbShippedDate.Size = new Size(74, 15);
            lbShippedDate.TabIndex = 8;
            lbShippedDate.Text = "ShippedDate";
            // 
            // lbRequiredDate
            // 
            lbRequiredDate.AutoSize = true;
            lbRequiredDate.Location = new Point(326, 81);
            lbRequiredDate.Name = "lbRequiredDate";
            lbRequiredDate.Size = new Size(78, 15);
            lbRequiredDate.TabIndex = 9;
            lbRequiredDate.Text = "RequiredDate";
            // 
            // dgvOrder
            // 
            dgvOrder.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvOrder.Location = new Point(10, 210);
            dgvOrder.Margin = new Padding(3, 2, 3, 2);
            dgvOrder.Name = "dgvOrder";
            dgvOrder.RowHeadersWidth = 51;
            dgvOrder.RowTemplate.Height = 29;
            dgvOrder.Size = new Size(679, 125);
            dgvOrder.TabIndex = 10;
            dgvOrder.DoubleClick += dgvOrder_DoubleClick;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(504, 173);
            btnDelete.Margin = new Padding(3, 2, 3, 2);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(82, 22);
            btnDelete.TabIndex = 11;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // btnEdit
            // 
            btnEdit.Location = new Point(304, 173);
            btnEdit.Margin = new Padding(3, 2, 3, 2);
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(82, 22);
            btnEdit.TabIndex = 12;
            btnEdit.Text = "Edit";
            btnEdit.UseVisualStyleBackColor = true;
            btnEdit.Click += btnEdit_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(99, 173);
            btnClose.Margin = new Padding(3, 2, 3, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(82, 22);
            btnClose.TabIndex = 14;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // txtShippedDate
            // 
            txtShippedDate.Location = new Point(423, 108);
            txtShippedDate.Margin = new Padding(3, 2, 3, 2);
            txtShippedDate.Name = "txtShippedDate";
            txtShippedDate.Size = new Size(163, 23);
            txtShippedDate.TabIndex = 15;
            // 
            // txtRequiredDate
            // 
            txtRequiredDate.Location = new Point(423, 76);
            txtRequiredDate.Margin = new Padding(3, 2, 3, 2);
            txtRequiredDate.Name = "txtRequiredDate";
            txtRequiredDate.Size = new Size(163, 23);
            txtRequiredDate.TabIndex = 16;
            // 
            // txtOrderDate
            // 
            txtOrderDate.Location = new Point(99, 142);
            txtOrderDate.Margin = new Padding(3, 2, 3, 2);
            txtOrderDate.Name = "txtOrderDate";
            txtOrderDate.Size = new Size(163, 23);
            txtOrderDate.TabIndex = 17;
            // 
            // txtMemberID
            // 
            txtMemberID.Location = new Point(99, 109);
            txtMemberID.Margin = new Padding(3, 2, 3, 2);
            txtMemberID.Name = "txtMemberID";
            txtMemberID.Size = new Size(163, 23);
            txtMemberID.TabIndex = 18;
            // 
            // txtOrderID
            // 
            txtOrderID.Location = new Point(99, 76);
            txtOrderID.Margin = new Padding(3, 2, 3, 2);
            txtOrderID.Name = "txtOrderID";
            txtOrderID.Size = new Size(163, 23);
            txtOrderID.TabIndex = 19;
            // 
            // lbFreight
            // 
            lbFreight.AutoSize = true;
            lbFreight.Location = new Point(326, 147);
            lbFreight.Name = "lbFreight";
            lbFreight.Size = new Size(44, 15);
            lbFreight.TabIndex = 20;
            lbFreight.Text = "Freight";
            // 
            // txtFreight
            // 
            txtFreight.Location = new Point(423, 142);
            txtFreight.Margin = new Padding(3, 2, 3, 2);
            txtFreight.Name = "txtFreight";
            txtFreight.Size = new Size(163, 23);
            txtFreight.TabIndex = 21;
            // 
            // frmManagerOrder
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 338);
            Controls.Add(txtFreight);
            Controls.Add(lbFreight);
            Controls.Add(txtOrderID);
            Controls.Add(txtMemberID);
            Controls.Add(txtOrderDate);
            Controls.Add(txtRequiredDate);
            Controls.Add(txtShippedDate);
            Controls.Add(btnClose);
            Controls.Add(btnEdit);
            Controls.Add(btnDelete);
            Controls.Add(dgvOrder);
            Controls.Add(lbRequiredDate);
            Controls.Add(lbShippedDate);
            Controls.Add(lbMemberID);
            Controls.Add(lbOrderDate);
            Controls.Add(lbOrderID);
            Controls.Add(btnSearch);
            Controls.Add(txtSearch);
            Controls.Add(cbxSearch);
            Controls.Add(btnOrderManager);
            Controls.Add(btnProductManager);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmManagerOrder";
            Text = "frmManagerOrder";
            Load += frmManagerOrder_Load;
            ((System.ComponentModel.ISupportInitialize)dgvOrder).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnProductManager;
        private Button btnOrderManager;
        private ComboBox cbxSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Label lbOrderID;
        private Label lbOrderDate;
        private Label lbMemberID;
        private Label lbShippedDate;
        private Label lbRequiredDate;
        private DataGridView dgvOrder;
        private Button btnDelete;
        private Button btnEdit;
        private Button btnClose;
        private TextBox txtShippedDate;
        private TextBox txtRequiredDate;
        private TextBox txtOrderDate;
        private TextBox txtMemberID;
        private TextBox txtOrderID;
        private Label lbFreight;
        private TextBox txtFreight;

        #endregion
    }
}