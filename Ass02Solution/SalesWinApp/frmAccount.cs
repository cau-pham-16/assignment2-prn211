﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessObject.BusinessObject;
using DataAccess;
using DataAccess.Repository;

namespace SalesWinApp
{
    public partial class frmAccount : Form
    {
        public frmAccount()
        {
            InitializeComponent();
        }
        public Member member { get; set; }
        IMemberRepositorycs memberRepositorycs = new MemberRepository();
        private void frmAccount_Load(object sender, EventArgs e)
        {
            txtMemID.Enabled = false;

            txtMemID.Text = member.MemberId.ToString();
            txtEmail.Text = member.Email;
            txtPass.Text = member.Password;
            txtCompany.Text = member.CompanyName;
            txtCity.Text = member.City;
            txtCountry.Text = member.Country;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            Member member = new Member
            {
                MemberId = int.Parse(txtMemID.Text),
                Email = txtEmail.Text,
                Password = txtPass.Text,
                CompanyName = txtCompany.Text,
                City = txtCity.Text,
                Country = txtCountry.Text
            };

            memberRepositorycs.Update(member);
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
