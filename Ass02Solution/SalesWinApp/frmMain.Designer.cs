﻿namespace SalesWinApp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnProductManager = new Button();
            btnOrderManager = new Button();
            cbxSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            lbMemId = new Label();
            lbEmail = new Label();
            lbCity = new Label();
            lbPass = new Label();
            lbCompany = new Label();
            dgvMember = new DataGridView();
            btnDelete = new Button();
            btnEdit = new Button();
            btnAdd = new Button();
            btnClose = new Button();
            txtPass = new TextBox();
            txtCompany = new TextBox();
            txtEmail = new TextBox();
            txtCity = new TextBox();
            txtMemID = new TextBox();
            lbCountry = new Label();
            txtCountry = new TextBox();
            ((System.ComponentModel.ISupportInitialize)dgvMember).BeginInit();
            SuspendLayout();
            // 
            // btnProductManager
            // 
            btnProductManager.Location = new Point(446, 9);
            btnProductManager.Margin = new Padding(3, 2, 3, 2);
            btnProductManager.Name = "btnProductManager";
            btnProductManager.Size = new Size(117, 26);
            btnProductManager.TabIndex = 0;
            btnProductManager.Text = "Product Manager";
            btnProductManager.UseVisualStyleBackColor = true;
            btnProductManager.Click += btnProductManager_Click;
            // 
            // btnOrderManager
            // 
            btnOrderManager.Location = new Point(579, 9);
            btnOrderManager.Margin = new Padding(3, 2, 3, 2);
            btnOrderManager.Name = "btnOrderManager";
            btnOrderManager.Size = new Size(110, 26);
            btnOrderManager.TabIndex = 1;
            btnOrderManager.Text = "Order Manager";
            btnOrderManager.UseVisualStyleBackColor = true;
            btnOrderManager.Click += btnOrderManager_Click;
            // 
            // cbxSearch
            // 
            cbxSearch.FormattingEnabled = true;
            cbxSearch.Items.AddRange(new object[] { "ID", "Name" });
            cbxSearch.Location = new Point(10, 14);
            cbxSearch.Margin = new Padding(3, 2, 3, 2);
            cbxSearch.Name = "cbxSearch";
            cbxSearch.Size = new Size(70, 23);
            cbxSearch.TabIndex = 2;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(86, 14);
            txtSearch.Margin = new Padding(3, 2, 3, 2);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(163, 23);
            txtSearch.TabIndex = 3;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(265, 15);
            btnSearch.Margin = new Padding(3, 2, 3, 2);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(82, 22);
            btnSearch.TabIndex = 4;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // lbMemId
            // 
            lbMemId.AutoSize = true;
            lbMemId.Location = new Point(10, 79);
            lbMemId.Name = "lbMemId";
            lbMemId.Size = new Size(63, 15);
            lbMemId.TabIndex = 5;
            lbMemId.Text = "MemberID";
            // 
            // lbEmail
            // 
            lbEmail.AutoSize = true;
            lbEmail.Location = new Point(10, 147);
            lbEmail.Name = "lbEmail";
            lbEmail.Size = new Size(36, 15);
            lbEmail.TabIndex = 6;
            lbEmail.Text = "Email";
            // 
            // lbCity
            // 
            lbCity.AutoSize = true;
            lbCity.Location = new Point(10, 111);
            lbCity.Name = "lbCity";
            lbCity.Size = new Size(28, 15);
            lbCity.TabIndex = 7;
            lbCity.Text = "City";
            // 
            // lbPass
            // 
            lbPass.AutoSize = true;
            lbPass.Location = new Point(326, 111);
            lbPass.Name = "lbPass";
            lbPass.Size = new Size(57, 15);
            lbPass.TabIndex = 8;
            lbPass.Text = "Password";
            // 
            // lbCompany
            // 
            lbCompany.AutoSize = true;
            lbCompany.Location = new Point(307, 81);
            lbCompany.Name = "lbCompany";
            lbCompany.Size = new Size(91, 15);
            lbCompany.TabIndex = 9;
            lbCompany.Text = "CompanyName";
            lbCompany.Click += lbCompany_Click;
            // 
            // dgvMember
            // 
            dgvMember.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvMember.Location = new Point(10, 210);
            dgvMember.Margin = new Padding(3, 2, 3, 2);
            dgvMember.Name = "dgvMember";
            dgvMember.RowHeadersWidth = 51;
            dgvMember.RowTemplate.Height = 29;
            dgvMember.Size = new Size(679, 125);
            dgvMember.TabIndex = 10;
            dgvMember.DoubleClick += dgvMember_DoubleClick;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(503, 173);
            btnDelete.Margin = new Padding(3, 2, 3, 2);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(82, 22);
            btnDelete.TabIndex = 11;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // btnEdit
            // 
            btnEdit.Location = new Point(207, 173);
            btnEdit.Margin = new Padding(3, 2, 3, 2);
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(82, 22);
            btnEdit.TabIndex = 12;
            btnEdit.Text = "Edit";
            btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            btnAdd.Location = new Point(354, 173);
            btnAdd.Margin = new Padding(3, 2, 3, 2);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(82, 22);
            btnAdd.TabIndex = 13;
            btnAdd.Text = "Create";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(73, 173);
            btnClose.Margin = new Padding(3, 2, 3, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(82, 22);
            btnClose.TabIndex = 14;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // txtPass
            // 
            txtPass.Location = new Point(423, 111);
            txtPass.Margin = new Padding(3, 2, 3, 2);
            txtPass.Name = "txtPass";
            txtPass.Size = new Size(163, 23);
            txtPass.TabIndex = 15;
            // 
            // txtCompany
            // 
            txtCompany.Location = new Point(423, 79);
            txtCompany.Margin = new Padding(3, 2, 3, 2);
            txtCompany.Name = "txtCompany";
            txtCompany.Size = new Size(163, 23);
            txtCompany.TabIndex = 16;
            // 
            // txtEmail
            // 
            txtEmail.Location = new Point(86, 142);
            txtEmail.Margin = new Padding(3, 2, 3, 2);
            txtEmail.Name = "txtEmail";
            txtEmail.Size = new Size(163, 23);
            txtEmail.TabIndex = 17;
            // 
            // txtCity
            // 
            txtCity.Location = new Point(86, 106);
            txtCity.Margin = new Padding(3, 2, 3, 2);
            txtCity.Name = "txtCity";
            txtCity.Size = new Size(163, 23);
            txtCity.TabIndex = 18;
            // 
            // txtMemID
            // 
            txtMemID.Location = new Point(86, 76);
            txtMemID.Margin = new Padding(3, 2, 3, 2);
            txtMemID.Name = "txtMemID";
            txtMemID.Size = new Size(163, 23);
            txtMemID.TabIndex = 19;
            // 
            // lbCountry
            // 
            lbCountry.AutoSize = true;
            lbCountry.Location = new Point(326, 147);
            lbCountry.Name = "lbCountry";
            lbCountry.Size = new Size(50, 15);
            lbCountry.TabIndex = 20;
            lbCountry.Text = "Country";
            // 
            // txtCountry
            // 
            txtCountry.Location = new Point(423, 148);
            txtCountry.Margin = new Padding(3, 2, 3, 2);
            txtCountry.Name = "txtCountry";
            txtCountry.Size = new Size(163, 23);
            txtCountry.TabIndex = 21;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 338);
            Controls.Add(txtCountry);
            Controls.Add(lbCountry);
            Controls.Add(txtMemID);
            Controls.Add(txtCity);
            Controls.Add(txtEmail);
            Controls.Add(txtCompany);
            Controls.Add(txtPass);
            Controls.Add(btnClose);
            Controls.Add(btnAdd);
            Controls.Add(btnEdit);
            Controls.Add(btnDelete);
            Controls.Add(dgvMember);
            Controls.Add(lbCompany);
            Controls.Add(lbPass);
            Controls.Add(lbCity);
            Controls.Add(lbEmail);
            Controls.Add(lbMemId);
            Controls.Add(btnSearch);
            Controls.Add(txtSearch);
            Controls.Add(cbxSearch);
            Controls.Add(btnOrderManager);
            Controls.Add(btnProductManager);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmMain";
            Text = "frmMain";
            Load += frmMain_Load;
            ((System.ComponentModel.ISupportInitialize)dgvMember).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnProductManager;
        private Button btnOrderManager;
        private ComboBox cbxSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Label lbMemId;
        private Label lbEmail;
        private Label lbCity;
        private Label lbPass;
        private Label lbCompany;
        private DataGridView dgvMember;
        private Button btnDelete;
        private Button btnEdit;
        private Button btnAdd;
        private Button btnClose;
        private TextBox txtPass;
        private TextBox txtCompany;
        private TextBox txtEmail;
        private TextBox txtCity;
        private TextBox txtMemID;
        private Label lbCountry;
        private TextBox txtCountry;
    }
}