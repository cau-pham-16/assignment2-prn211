﻿using BusinessObject.BusinessObject;
using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinApp
{
    public partial class frmManagerProduct : Form
    {
        public frmManagerProduct()
        {
            InitializeComponent();
        }
        IProductRepository productRepository = new ProductRepository();
        BindingSource source;
        private void btnProductManager_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void frmManagerProduct_Load(object sender, EventArgs e)
        {
            List<Product> products = new List<Product>();
            btnDelete.Enabled = false;
            try
            {
                products = (List<Product>)productRepository.GetAllProduct();
                source = new BindingSource();
                source.DataSource = products;
                txtProductID.DataBindings.Clear();
                txtCategoryID.DataBindings.Clear();
                txtProductName.DataBindings.Clear();
                txtUnitPrice.DataBindings.Clear();
                txtUnitSln.DataBindings.Clear();
                txtWeight.DataBindings.Clear();

                txtProductID.DataBindings.Add("Text", source, "ProductId");
                txtCategoryID.DataBindings.Add("Text", source, "CategoryId");
                txtProductName.DataBindings.Add("Text", source, "ProductName");
                txtWeight.DataBindings.Add("Text", source, "Weight");
                txtUnitPrice.DataBindings.Add("Text", source, "UnitPrice");
                txtUnitSln.DataBindings.Add("Text", source, "UnitslnStock");
                dgvProduct.DataSource = null;
                dgvProduct.DataSource = source;
                if (products.Count() == 0)
                {
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Show list Product");
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            frmProduct fm = new frmProduct
            {
                Text = "Create Product",
                InsertOrUpdate = false,
                productRepository = productRepository
            };
            if (fm.ShowDialog() == DialogResult.OK)
            {
                fm.Close();
                fm.Hide();
                frmManagerProduct_Load(sender, e);
            }
        }

        private void dgvProduct_DoubleClick(object sender, EventArgs e)
        {
            frmProduct fm = new frmProduct
            {
                Text = "Edit Member",
                InsertOrUpdate = true,
                product = GetProduct(),
                productRepository = productRepository
            };
            if (fm.ShowDialog() == DialogResult.OK)
            {
                fm.Close();
                fm.Hide();
                frmManagerProduct_Load(sender, e);
            }
        }

        private Product GetProduct()
        {
            Product p = new Product();
            try
            {
                p = new Product
                {
                    ProductId = int.Parse(txtProductID.Text),
                    CategoryId = int.Parse(txtCategoryID.Text),
                    ProductName = txtProductName.Text,
                    Weight = txtWeight.Text,
                    UnitPrice = decimal.Parse(txtUnitPrice.Text),
                    UnitslnStock = int.Parse(txtUnitSln.Text)
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Product");
            }
            return p;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Product p = GetProduct();
            productRepository.DeleteProduct(p.ProductId);
            frmManagerProduct_Load(sender, e);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String filter = cbxSearch.Text;
            String key = txtSearch.Text;
            List<Product> products = new List<Product>();
            ProductDAO md = new ProductDAO();
            products = md.SearchProduct(filter, key);
            source = new BindingSource();
            source.DataSource = products;
            txtProductID.DataBindings.Clear();
            txtCategoryID.DataBindings.Clear();
            txtProductName.DataBindings.Clear();
            txtUnitPrice.DataBindings.Clear();
            txtUnitSln.DataBindings.Clear();
            txtWeight.DataBindings.Clear();

            txtProductID.DataBindings.Add("Text", source, "ProductId");
            txtCategoryID.DataBindings.Add("Text", source, "CategoryId");
            txtProductName.DataBindings.Add("Text", source, "ProductName");
            txtUnitPrice.DataBindings.Add("Text", source, "UnitPrice");
            txtWeight.DataBindings.Add("Text", source, "Weight");
            txtUnitSln.DataBindings.Add("Text", source, "UnitslnStock");
            dgvProduct.DataSource = null;
            dgvProduct.DataSource = source;
        }

        private void btnOrderManager_Click(object sender, EventArgs e)
        {
            frmManagerOrder fm = new frmManagerOrder();
            this.Hide();
            fm.ShowDialog();
            this.Close();
        }
    }
}
