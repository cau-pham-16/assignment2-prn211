﻿using BusinessObject.BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccess;
using System.Globalization;

namespace SalesWinApp
{
    public partial class frmManagerOrder : Form
    {
        public frmManagerOrder()
        {
            InitializeComponent();
        }
        BindingSource source;
        IOrderRepository orderRepository = new OrderRepository();
        private void dgvOrder_DoubleClick(object sender, EventArgs e)
        {
            Order order = GetOrder();
            frmOrderDetail fm = new frmOrderDetail
            {
                orderId = order.OrderId,
                orderDetailRepository = new OrderDetailrepository()
            };
            fm.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Order o = GetOrder();
            if (o != null)
            {
                OrderDetailrepository orderDetailrepository = new OrderDetailrepository();
                List<OrderDetail> orderDetails = orderDetailrepository.GetOredrDetail(o.OrderId);
                foreach (OrderDetail orderDetail in orderDetails)
                {
                    orderDetailrepository.DeleteOrderDetail(orderDetail);
                }
                orderRepository.DeleteOrder(o.OrderId);
            }
            frmManagerOrder_Load(sender, e);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var o = GetOrder();
                if (o != null)
                {
                    frmOrder frm = new frmOrder
                    {
                        order = o,
                    };
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        frm.Close();
                        frmManagerOrder_Load(sender, e);
                        this.Show();
                    }

                }
                else
                {
                    MessageBox.Show("Select Order");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            String filter = cbxSearch.Text;
            String key = txtSearch.Text;
            List<Order> oredrs = new List<Order>();
            OrderDAO od = new OrderDAO();
            oredrs = od.SearchOrder(filter, key);
            source = new BindingSource();
            source.DataSource = oredrs;
            txtOrderID.DataBindings.Clear();
            txtMemberID.DataBindings.Clear();
            txtOrderDate.DataBindings.Clear();
            txtRequiredDate.DataBindings.Clear();
            txtShippedDate.DataBindings.Clear();
            txtFreight.DataBindings.Clear();

            txtOrderID.DataBindings.Add("Text", source, "OrderId");
            txtMemberID.DataBindings.Add("Text", source, "MemberId");
            txtOrderDate.DataBindings.Add("Text", source, "OrderDate");
            txtRequiredDate.DataBindings.Add("Text", source, "ShippedDate");
            txtShippedDate.DataBindings.Add("Text", source, "RequiredDate");
            txtFreight.DataBindings.Add("Text", source, "Freight");
            dgvOrder.DataSource = null;
            dgvOrder.DataSource = source;
        }

        private void btnOrderManager_Click(object sender, EventArgs e)
        {
            this.Show();
        }

        private void frmManagerOrder_Load(object sender, EventArgs e)
        {
            List<Order> orders = new List<Order>();
            btnDelete.Enabled = false;
            try
            {
                orders = (List<Order>)orderRepository.GetAllOrder();
                source = new BindingSource();
                source.DataSource = orders;
                txtOrderID.DataBindings.Clear();
                txtMemberID.DataBindings.Clear();
                txtOrderDate.DataBindings.Clear();
                txtRequiredDate.DataBindings.Clear();
                txtShippedDate.DataBindings.Clear();
                txtFreight.DataBindings.Clear();

                txtOrderID.DataBindings.Add("Text", source, "OrderId");
                txtMemberID.DataBindings.Add("Text", source, "MemberId");
                txtOrderDate.DataBindings.Add("Text", source, "OrderDate");
                txtRequiredDate.DataBindings.Add("Text", source, "ShippedDate");
                txtShippedDate.DataBindings.Add("Text", source, "RequiredDate");
                txtFreight.DataBindings.Add("Text", source, "Freight");
                dgvOrder.DataSource = null;
                dgvOrder.DataSource = source;
                if (orders.Count() == 0)
                {
                    btnDelete.Enabled = false;
                }
                else
                {
                    btnDelete.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Show list Product");
            }
        }

        private Order GetOrder()
        {
            Order o = new Order();
            try
            {
                o = new Order
                {
                    OrderId = int.Parse(txtOrderID.Text),
                    MemberId = int.Parse(txtMemberID.Text),
                    OrderDate = DateTime.Parse(txtOrderDate.Text),
                    ShippedDate = DateTime.Parse(txtShippedDate.Text),
                    Freight = decimal.TryParse(txtFreight.Text, out decimal freight) ? freight : (decimal?)null
                };

                if (!string.IsNullOrWhiteSpace(txtRequiredDate.Text))
                {
                    o.RequiredDate = DateTime.Parse(txtRequiredDate.Text);
                }
                else
                {
                    o.RequiredDate = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Get Product");
            }
            return o;
        }

        private void btnProductManager_Click(object sender, EventArgs e)
        {
            frmManagerProduct fm = new frmManagerProduct();
            this.Hide();
            fm.ShowDialog();
            this.Close();
        }
    }
}
