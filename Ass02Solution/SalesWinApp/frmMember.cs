﻿using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessObject.BusinessObject;
using System.Reflection;

namespace SalesWinApp
{
    public partial class frmMember : Form
    {
        public frmMember()
        {
            InitializeComponent();
        }
        public bool InsertOrUpdate { get; set; }
        public Member MemberInfor { get; set; }
        public IMemberRepositorycs memberRepository { get; set; }
        private void frmMember_Load(object sender, EventArgs e)
        {
            txtMemID.Enabled = !InsertOrUpdate;
            if (InsertOrUpdate == true)
            {
                txtMemID.Text = MemberInfor.MemberId.ToString();
                txtEmail.Text = MemberInfor.Email;
                txtPass.Text = MemberInfor.Password;
                txtCompany.Text = MemberInfor.CompanyName;
                txtCity.Text = MemberInfor.City;
                txtCountry.Text = MemberInfor.Country;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Member member = new Member
                {
                    MemberId = int.Parse(txtMemID.Text),
                    Email = txtEmail.Text,
                    Password = txtPass.Text,
                    CompanyName = txtCompany.Text,
                    City = txtCity.Text,
                    Country = txtCountry.Text
                };
                if (InsertOrUpdate == true)
                {
                    memberRepository.Update(member);
                }
                else
                {
                    memberRepository.InsertMember(member);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, InsertOrUpdate == true ? "UpdateMember" : "Add Member");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
