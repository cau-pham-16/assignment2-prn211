﻿using BusinessObject.BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinApp
{
    public partial class frmOrderDetail : Form
    {
        public frmOrderDetail()
        {
            InitializeComponent();
        }
        public int orderId { get; set; }
        public IOrderDetailRepository orderDetailRepository { get; set; }
        BindingSource source = new BindingSource();
        private void btnSave_Click(object sender, EventArgs e)
        {
            OrderDetail od = new OrderDetail
            {
                OrderId = int.Parse(txtOrderID.Text),
                ProductId = int.Parse(txtProductID.Text),
                UnitPrice = decimal.Parse(txtUnitPrice.Text),
                Quantity = int.Parse(txtQuantity.Text),
                Discount = double.Parse(txtDiscount.Text),
            };
            if (od != null)
            {
                orderDetailRepository.UpdateOrderDetail(od);
            }
            else
            {
                MessageBox.Show("Update OrderDetail");
            }
            frmOrderDetail_Load(sender, e);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmOrderDetail_Load(object sender, EventArgs e)
        {
            txtOrderID.Enabled = false;
            txtProductID.Enabled = false;
            txtQuantity.Enabled = false;
            txtUnitPrice.Enabled = false;
            List<OrderDetail> orderDetails = orderDetailRepository.GetOredrDetail(orderId);
            source.DataSource = orderDetails;

            dgvOrderDetail.DataSource = null;
            dgvOrderDetail.DataSource = source;
        }

        private void lbOrderID_Click(object sender, EventArgs e)
        {

        }

        private void dgvOrderDetail_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = dgvOrderDetail.CurrentRow;

            txtOrderID.Text = selectedRow.Cells["OrderID"].Value.ToString();
            txtProductID.Text = selectedRow.Cells["ProductId"].Value.ToString();
            txtQuantity.Text = selectedRow.Cells["Quantity"].Value.ToString();
            txtUnitPrice.Text = selectedRow.Cells["UnitPrice"].Value.ToString();
            txtDiscount.Text = selectedRow.Cells["Discount"].Value.ToString();
        }

        private OrderDetail txtDiscount_TextChanged(object sender, EventArgs e)
        {
            OrderDetail orderDetail = new OrderDetail
            {
                OrderId = int.Parse(txtOrderID.Text),
                ProductId = int.Parse(txtProductID.Text),
                UnitPrice = decimal.Parse(txtUnitPrice.Text),
                Quantity = int.Parse(txtQuantity.Text),
                Discount = double.Parse(txtDiscount.Text),
            };
            return orderDetail;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            OrderDetail od = txtDiscount_TextChanged(sender, e);
            orderDetailRepository.DeleteOrderDetail(od);
            frmOrderDetail_Load(sender, e);
        }
    }
}
