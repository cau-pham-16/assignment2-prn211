﻿namespace SalesWinApp
{
    partial class frmManagerProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnProductManager = new Button();
            btnOrderManager = new Button();
            cbxSearch = new ComboBox();
            txtSearch = new TextBox();
            btnSearch = new Button();
            lbProductID = new Label();
            lbCategoryID = new Label();
            lbProductName = new Label();
            lbUnitPrice = new Label();
            lbWeight = new Label();
            dgvProduct = new DataGridView();
            btnDelete = new Button();
            btnEdit = new Button();
            btnAdd = new Button();
            btnClose = new Button();
            txtUnitPrice = new TextBox();
            txtWeight = new TextBox();
            txtCategoryID = new TextBox();
            txtProductName = new TextBox();
            txtProductID = new TextBox();
            lbUnitSln = new Label();
            txtUnitSln = new TextBox();
            ((System.ComponentModel.ISupportInitialize)dgvProduct).BeginInit();
            SuspendLayout();
            // 
            // btnProductManager
            // 
            btnProductManager.Location = new Point(446, 9);
            btnProductManager.Margin = new Padding(3, 2, 3, 2);
            btnProductManager.Name = "btnProductManager";
            btnProductManager.Size = new Size(117, 26);
            btnProductManager.TabIndex = 0;
            btnProductManager.Text = "Product Manager";
            btnProductManager.UseVisualStyleBackColor = true;
            btnProductManager.Click += btnProductManager_Click;
            // 
            // btnOrderManager
            // 
            btnOrderManager.Location = new Point(579, 9);
            btnOrderManager.Margin = new Padding(3, 2, 3, 2);
            btnOrderManager.Name = "btnOrderManager";
            btnOrderManager.Size = new Size(110, 26);
            btnOrderManager.TabIndex = 1;
            btnOrderManager.Text = "Order Manager";
            btnOrderManager.UseVisualStyleBackColor = true;
            btnOrderManager.Click += btnOrderManager_Click;
            // 
            // cbxSearch
            // 
            cbxSearch.FormattingEnabled = true;
            cbxSearch.Items.AddRange(new object[] { "ID", "Name" });
            cbxSearch.Location = new Point(10, 14);
            cbxSearch.Margin = new Padding(3, 2, 3, 2);
            cbxSearch.Name = "cbxSearch";
            cbxSearch.Size = new Size(70, 23);
            cbxSearch.TabIndex = 2;
            // 
            // txtSearch
            // 
            txtSearch.Location = new Point(86, 16);
            txtSearch.Margin = new Padding(3, 2, 3, 2);
            txtSearch.Name = "txtSearch";
            txtSearch.Size = new Size(163, 23);
            txtSearch.TabIndex = 3;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(250, 14);
            btnSearch.Margin = new Padding(3, 2, 3, 2);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(82, 22);
            btnSearch.TabIndex = 4;
            btnSearch.Text = "Search";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // lbProductID
            // 
            lbProductID.AutoSize = true;
            lbProductID.Location = new Point(3, 81);
            lbProductID.Name = "lbProductID";
            lbProductID.Size = new Size(60, 15);
            lbProductID.TabIndex = 5;
            lbProductID.Text = "ProductID";
            // 
            // lbCategoryID
            // 
            lbCategoryID.AutoSize = true;
            lbCategoryID.Location = new Point(3, 147);
            lbCategoryID.Name = "lbCategoryID";
            lbCategoryID.Size = new Size(66, 15);
            lbCategoryID.TabIndex = 6;
            lbCategoryID.Text = "CategoryID";
            // 
            // lbProductName
            // 
            lbProductName.AutoSize = true;
            lbProductName.Location = new Point(3, 114);
            lbProductName.Name = "lbProductName";
            lbProductName.Size = new Size(84, 15);
            lbProductName.TabIndex = 7;
            lbProductName.Text = "Product Name";
            // 
            // lbUnitPrice
            // 
            lbUnitPrice.AutoSize = true;
            lbUnitPrice.Location = new Point(326, 113);
            lbUnitPrice.Name = "lbUnitPrice";
            lbUnitPrice.Size = new Size(55, 15);
            lbUnitPrice.TabIndex = 8;
            lbUnitPrice.Text = "UnitPrice";
            // 
            // lbWeight
            // 
            lbWeight.AutoSize = true;
            lbWeight.Location = new Point(326, 81);
            lbWeight.Name = "lbWeight";
            lbWeight.Size = new Size(45, 15);
            lbWeight.TabIndex = 9;
            lbWeight.Text = "Weight";
            // 
            // dgvProduct
            // 
            dgvProduct.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvProduct.Location = new Point(10, 210);
            dgvProduct.Margin = new Padding(3, 2, 3, 2);
            dgvProduct.Name = "dgvProduct";
            dgvProduct.RowHeadersWidth = 51;
            dgvProduct.RowTemplate.Height = 29;
            dgvProduct.Size = new Size(679, 125);
            dgvProduct.TabIndex = 10;
            dgvProduct.DoubleClick += dgvProduct_DoubleClick;
            // 
            // btnDelete
            // 
            btnDelete.Location = new Point(504, 173);
            btnDelete.Margin = new Padding(3, 2, 3, 2);
            btnDelete.Name = "btnDelete";
            btnDelete.Size = new Size(82, 22);
            btnDelete.TabIndex = 11;
            btnDelete.Text = "Delete";
            btnDelete.UseVisualStyleBackColor = true;
            btnDelete.Click += btnDelete_Click;
            // 
            // btnEdit
            // 
            btnEdit.Location = new Point(207, 173);
            btnEdit.Margin = new Padding(3, 2, 3, 2);
            btnEdit.Name = "btnEdit";
            btnEdit.Size = new Size(82, 22);
            btnEdit.TabIndex = 12;
            btnEdit.Text = "Edit";
            btnEdit.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            btnAdd.Location = new Point(354, 173);
            btnAdd.Margin = new Padding(3, 2, 3, 2);
            btnAdd.Name = "btnAdd";
            btnAdd.Size = new Size(82, 22);
            btnAdd.TabIndex = 13;
            btnAdd.Text = "Create";
            btnAdd.UseVisualStyleBackColor = true;
            btnAdd.Click += btnAdd_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(73, 173);
            btnClose.Margin = new Padding(3, 2, 3, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(82, 22);
            btnClose.TabIndex = 14;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // txtUnitPrice
            // 
            txtUnitPrice.Location = new Point(423, 108);
            txtUnitPrice.Margin = new Padding(3, 2, 3, 2);
            txtUnitPrice.Name = "txtUnitPrice";
            txtUnitPrice.Size = new Size(163, 23);
            txtUnitPrice.TabIndex = 15;
            // 
            // txtWeight
            // 
            txtWeight.Location = new Point(423, 76);
            txtWeight.Margin = new Padding(3, 2, 3, 2);
            txtWeight.Name = "txtWeight";
            txtWeight.Size = new Size(163, 23);
            txtWeight.TabIndex = 16;
            // 
            // txtCategoryID
            // 
            txtCategoryID.Location = new Point(99, 142);
            txtCategoryID.Margin = new Padding(3, 2, 3, 2);
            txtCategoryID.Name = "txtCategoryID";
            txtCategoryID.Size = new Size(163, 23);
            txtCategoryID.TabIndex = 17;
            // 
            // txtProductName
            // 
            txtProductName.Location = new Point(99, 109);
            txtProductName.Margin = new Padding(3, 2, 3, 2);
            txtProductName.Name = "txtProductName";
            txtProductName.Size = new Size(163, 23);
            txtProductName.TabIndex = 18;
            // 
            // txtProductID
            // 
            txtProductID.Location = new Point(99, 76);
            txtProductID.Margin = new Padding(3, 2, 3, 2);
            txtProductID.Name = "txtProductID";
            txtProductID.Size = new Size(163, 23);
            txtProductID.TabIndex = 19;
            // 
            // lbUnitSln
            // 
            lbUnitSln.AutoSize = true;
            lbUnitSln.Location = new Point(326, 147);
            lbUnitSln.Name = "lbUnitSln";
            lbUnitSln.Size = new Size(74, 15);
            lbUnitSln.TabIndex = 20;
            lbUnitSln.Text = "UnitSlnStock";
            // 
            // txtUnitSln
            // 
            txtUnitSln.Location = new Point(423, 142);
            txtUnitSln.Margin = new Padding(3, 2, 3, 2);
            txtUnitSln.Name = "txtUnitSln";
            txtUnitSln.Size = new Size(163, 23);
            txtUnitSln.TabIndex = 21;
            // 
            // frmManagerProduct
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(700, 338);
            Controls.Add(txtUnitSln);
            Controls.Add(lbUnitSln);
            Controls.Add(txtProductID);
            Controls.Add(txtProductName);
            Controls.Add(txtCategoryID);
            Controls.Add(txtWeight);
            Controls.Add(txtUnitPrice);
            Controls.Add(btnClose);
            Controls.Add(btnAdd);
            Controls.Add(btnEdit);
            Controls.Add(btnDelete);
            Controls.Add(dgvProduct);
            Controls.Add(lbWeight);
            Controls.Add(lbUnitPrice);
            Controls.Add(lbProductName);
            Controls.Add(lbCategoryID);
            Controls.Add(lbProductID);
            Controls.Add(btnSearch);
            Controls.Add(txtSearch);
            Controls.Add(cbxSearch);
            Controls.Add(btnOrderManager);
            Controls.Add(btnProductManager);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmManagerProduct";
            Text = "frmManagerProduct";
            Load += frmManagerProduct_Load;
            ((System.ComponentModel.ISupportInitialize)dgvProduct).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnProductManager;
        private Button btnOrderManager;
        private ComboBox cbxSearch;
        private TextBox txtSearch;
        private Button btnSearch;
        private Label lbProductID;
        private Label lbCategoryID;
        private Label lbProductName;
        private Label lbUnitPrice;
        private Label lbWeight;
        private DataGridView dgvProduct;
        private Button btnDelete;
        private Button btnEdit;
        private Button btnAdd;
        private Button btnClose;
        private TextBox txtUnitPrice;
        private TextBox txtWeight;
        private TextBox txtCategoryID;
        private TextBox txtProductName;
        private TextBox txtProductID;
        private Label lbUnitSln;
        private TextBox txtUnitSln;
    }
}
