﻿namespace SalesWinApp
{
    partial class frmProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnSave = new Button();
            btnCancel = new Button();
            lbProductID = new Label();
            lbProductName = new Label();
            lbCategoryID = new Label();
            lbWeight = new Label();
            lbUnitPrice = new Label();
            lbUnitSln = new Label();
            txtProductID = new TextBox();
            txtProductName = new TextBox();
            txtCategoryID = new TextBox();
            txtWeight = new TextBox();
            txtUnitPrice = new TextBox();
            txtUnitSln = new TextBox();
            SuspendLayout();
            // 
            // btnSave
            // 
            btnSave.DialogResult = DialogResult.OK;
            btnSave.Location = new Point(278, 278);
            btnSave.Margin = new Padding(3, 2, 3, 2);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(82, 22);
            btnSave.TabIndex = 0;
            btnSave.Text = "Save";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += btnSave_Click;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = DialogResult.Cancel;
            btnCancel.Location = new Point(90, 278);
            btnCancel.Margin = new Padding(3, 2, 3, 2);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new Size(82, 22);
            btnCancel.TabIndex = 1;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Click += btnCancel_Click;
            // 
            // lbProductID
            // 
            lbProductID.AutoSize = true;
            lbProductID.Location = new Point(60, 27);
            lbProductID.Name = "lbProductID";
            lbProductID.Size = new Size(60, 15);
            lbProductID.TabIndex = 2;
            lbProductID.Text = "ProductID";
            // 
            // lbProductName
            // 
            lbProductName.AutoSize = true;
            lbProductName.Location = new Point(60, 75);
            lbProductName.Name = "lbProductName";
            lbProductName.Size = new Size(81, 15);
            lbProductName.TabIndex = 3;
            lbProductName.Text = "ProductName";
            // 
            // lbCategoryID
            // 
            lbCategoryID.AutoSize = true;
            lbCategoryID.Location = new Point(60, 116);
            lbCategoryID.Name = "lbCategoryID";
            lbCategoryID.Size = new Size(66, 15);
            lbCategoryID.TabIndex = 4;
            lbCategoryID.Text = "CategoryID";
            // 
            // lbWeight
            // 
            lbWeight.AutoSize = true;
            lbWeight.Location = new Point(60, 154);
            lbWeight.Name = "lbWeight";
            lbWeight.Size = new Size(45, 15);
            lbWeight.TabIndex = 5;
            lbWeight.Text = "Weight";
            // 
            // lbUnitPrice
            // 
            lbUnitPrice.AutoSize = true;
            lbUnitPrice.Location = new Point(60, 194);
            lbUnitPrice.Name = "lbUnitPrice";
            lbUnitPrice.Size = new Size(55, 15);
            lbUnitPrice.TabIndex = 6;
            lbUnitPrice.Text = "UnitPrice";
            // 
            // lbUnitSln
            // 
            lbUnitSln.AutoSize = true;
            lbUnitSln.Location = new Point(60, 230);
            lbUnitSln.Name = "lbUnitSln";
            lbUnitSln.Size = new Size(73, 15);
            lbUnitSln.TabIndex = 7;
            lbUnitSln.Text = "UnitslnStock";
            // 
            // txtProductID
            // 
            txtProductID.Location = new Point(175, 28);
            txtProductID.Margin = new Padding(3, 2, 3, 2);
            txtProductID.Name = "txtProductID";
            txtProductID.Size = new Size(186, 23);
            txtProductID.TabIndex = 8;
            // 
            // txtProductName
            // 
            txtProductName.Location = new Point(175, 73);
            txtProductName.Margin = new Padding(3, 2, 3, 2);
            txtProductName.Name = "txtProductName";
            txtProductName.Size = new Size(186, 23);
            txtProductName.TabIndex = 9;
            // 
            // txtCategoryID
            // 
            txtCategoryID.Location = new Point(175, 113);
            txtCategoryID.Margin = new Padding(3, 2, 3, 2);
            txtCategoryID.Name = "txtCategoryID";
            txtCategoryID.Size = new Size(186, 23);
            txtCategoryID.TabIndex = 10;
            // 
            // txtWeight
            // 
            txtWeight.Location = new Point(175, 152);
            txtWeight.Margin = new Padding(3, 2, 3, 2);
            txtWeight.Name = "txtWeight";
            txtWeight.Size = new Size(186, 23);
            txtWeight.TabIndex = 11;
            // 
            // txtUnitPrice
            // 
            txtUnitPrice.Location = new Point(175, 189);
            txtUnitPrice.Margin = new Padding(3, 2, 3, 2);
            txtUnitPrice.Name = "txtUnitPrice";
            txtUnitPrice.Size = new Size(186, 23);
            txtUnitPrice.TabIndex = 12;
            // 
            // txtUnitSln
            // 
            txtUnitSln.Location = new Point(175, 222);
            txtUnitSln.Margin = new Padding(3, 2, 3, 2);
            txtUnitSln.Name = "txtUnitSln";
            txtUnitSln.Size = new Size(186, 23);
            txtUnitSln.TabIndex = 13;
            // 
            // frmProduct
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(452, 340);
            Controls.Add(txtUnitSln);
            Controls.Add(txtUnitPrice);
            Controls.Add(txtWeight);
            Controls.Add(txtCategoryID);
            Controls.Add(txtProductName);
            Controls.Add(txtProductID);
            Controls.Add(lbUnitSln);
            Controls.Add(lbUnitPrice);
            Controls.Add(lbWeight);
            Controls.Add(lbCategoryID);
            Controls.Add(lbProductName);
            Controls.Add(lbProductID);
            Controls.Add(btnCancel);
            Controls.Add(btnSave);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmProduct";
            Text = "frmMember";
            Load += frmProduct_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnSave;
        private Button btnCancel;
        private Label lbProductID;
        private Label lbProductName;
        private Label lbCategoryID;
        private Label lbWeight;
        private Label lbUnitPrice;
        private Label lbUnitSln;
        private TextBox txtProductID;
        private TextBox txtProductName;
        private TextBox txtCategoryID;
        private TextBox txtWeight;
        private TextBox txtUnitPrice;
        private TextBox txtUnitSln;
    }
}