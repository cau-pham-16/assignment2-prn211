﻿using BusinessObject.BusinessObject;
using DataAccess;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinApp
{
    public partial class frmCustomer : Form
    {
        public frmCustomer()
        {
            InitializeComponent();
        }
        public List<OrderDetail> list = new List<OrderDetail>();
        BindingSource source;
        public Member member { get; set; }
        IProductRepository productRepository = new ProductRepository();
        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmCustomer_Load(object sender, EventArgs e)
        {

            source = new BindingSource();
            source.DataSource = productRepository.GetAllProduct();
            dgvProduct.DataSource = null;
            dgvProduct.DataSource = source;
        }

        private void dgvProduct_DoubleClick(object sender, EventArgs e)
        {
            Product p = GetProduct(sender, e);
            txtProductName.Text = p.ProductName;
            txtUnitPrice.Text = p.UnitPrice.ToString();
            txtQuantity.Text = "1";
        }
        private Product GetProduct(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = dgvProduct.CurrentRow;
            Product p = new Product();
            if (selectedRow != null)
            {
                p.ProductId = int.Parse(selectedRow.Cells["ProductId"].Value.ToString());
                p.CategoryId = int.Parse(selectedRow.Cells["CategoryId"].Value.ToString());
                p.ProductName = selectedRow.Cells["ProductName"].Value.ToString();
                p.Weight = selectedRow.Cells["Weight"].Value.ToString();
                p.UnitPrice = decimal.Parse(selectedRow.Cells["UnitPrice"].Value.ToString());
                p.UnitslnStock = int.Parse(selectedRow.Cells["UnitslnStock"].Value.ToString());

            }
            return p;
        }

        private void btnChoose_Click(object sender, EventArgs e)
        {
            Product product = GetProduct(sender, e);
            int quantity = int.Parse(txtQuantity.Text);
            OrderDetail detail = new OrderDetail();
            detail.ProductId = product.ProductId;
            detail.Quantity = quantity;
            detail.UnitPrice = product.UnitPrice;
            detail.Discount = 0;
            list.Add(detail);
            dgvChoose.AutoGenerateColumns = false;
            DataGridViewTextBoxColumn productIdColumn = new DataGridViewTextBoxColumn();
            productIdColumn.DataPropertyName = "ProductId"; // Tên thuộc tính trong đối tượng OrderDetail tương ứng với cột này
            productIdColumn.HeaderText = "Product ID";

            DataGridViewTextBoxColumn UnitPrice = new DataGridViewTextBoxColumn();
            UnitPrice.DataPropertyName = "UnitPrice"; // Tên thuộc tính trong đối tượng OrderDetail tương ứng với cột này
            UnitPrice.HeaderText = "Unit Price";

            // Xác định cột Quantity
            DataGridViewTextBoxColumn quantityColumn = new DataGridViewTextBoxColumn();
            quantityColumn.DataPropertyName = "Quantity"; // Tên thuộc tính trong đối tượng OrderDetail tương ứng với cột này
            quantityColumn.HeaderText = "Quantity";

            // Thêm các cột vào DataGridView
            dgvChoose.Columns.Add(productIdColumn);
            dgvChoose.Columns.Add(quantityColumn);
            dgvChoose.Columns.Add(UnitPrice);
            dgvChoose.DataSource = list;
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            IOrderRepository orderRepository = new OrderRepository();
            OrderDAO od = new OrderDAO();
            IOrderDetailRepository orderDetailRepository = new OrderDetailrepository();
            Order o = new Order();
            DateTime OrderDate = DateTime.Now;
            DateTime ShippedDate = OrderDate.AddDays(7);
            o.OrderDate = OrderDate;
            o.ShippedDate = ShippedDate;
            o.RequiredDate = OrderDate;
            int lastOrderId = od.GetLastOrderId();
            o.OrderId = lastOrderId + 1;
            o.MemberId = member.MemberId;
            orderRepository.InsertOrder(o);
            foreach (var i in list)
            {
                i.OrderId = o.OrderId;
            }
            orderDetailRepository.InsertOrderDetail(list);
            list = null;
            dgvChoose.DataSource = list;
            MessageBox.Show("Buy Successful");
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            frmAccount frmAccount = new frmAccount
            {
                member = member,
            };

            if (frmAccount.ShowDialog() == DialogResult.OK)
            {
                this.Show();
                MessageBox.Show("Update Successful!");
            }

        }
    }
}
