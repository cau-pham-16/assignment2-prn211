﻿namespace SalesWinApp
{
    partial class frmCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnAccount = new Button();
            btnChoose = new Button();
            btnBuy = new Button();
            btnClose = new Button();
            lbProductName = new Label();
            lbUnitPrice = new Label();
            lbQuantity = new Label();
            dgvProduct = new DataGridView();
            dgvChoose = new DataGridView();
            txtQuantity = new TextBox();
            txtProductName = new TextBox();
            txtUnitPrice = new TextBox();
            ((System.ComponentModel.ISupportInitialize)dgvProduct).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dgvChoose).BeginInit();
            SuspendLayout();
            // 
            // btnAccount
            // 
            btnAccount.Location = new Point(883, 22);
            btnAccount.Margin = new Padding(3, 2, 3, 2);
            btnAccount.Name = "btnAccount";
            btnAccount.Size = new Size(82, 22);
            btnAccount.TabIndex = 0;
            btnAccount.Text = "Account";
            btnAccount.UseVisualStyleBackColor = true;
            btnAccount.Click += btnAccount_Click;
            // 
            // btnChoose
            // 
            btnChoose.Location = new Point(823, 176);
            btnChoose.Margin = new Padding(3, 2, 3, 2);
            btnChoose.Name = "btnChoose";
            btnChoose.Size = new Size(82, 22);
            btnChoose.TabIndex = 1;
            btnChoose.Text = "Choose";
            btnChoose.UseVisualStyleBackColor = true;
            btnChoose.Click += btnChoose_Click;
            // 
            // btnBuy
            // 
            btnBuy.Location = new Point(661, 176);
            btnBuy.Margin = new Padding(3, 2, 3, 2);
            btnBuy.Name = "btnBuy";
            btnBuy.Size = new Size(82, 22);
            btnBuy.TabIndex = 2;
            btnBuy.Text = "Buy";
            btnBuy.UseVisualStyleBackColor = true;
            btnBuy.Click += btnBuy_Click;
            // 
            // btnClose
            // 
            btnClose.Location = new Point(494, 176);
            btnClose.Margin = new Padding(3, 2, 3, 2);
            btnClose.Name = "btnClose";
            btnClose.Size = new Size(82, 22);
            btnClose.TabIndex = 3;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            btnClose.Click += btnClose_Click;
            // 
            // lbProductName
            // 
            lbProductName.AutoSize = true;
            lbProductName.Location = new Point(400, 59);
            lbProductName.Name = "lbProductName";
            lbProductName.Size = new Size(81, 15);
            lbProductName.TabIndex = 4;
            lbProductName.Text = "ProductName";
            // 
            // lbUnitPrice
            // 
            lbUnitPrice.AutoSize = true;
            lbUnitPrice.Location = new Point(400, 111);
            lbUnitPrice.Name = "lbUnitPrice";
            lbUnitPrice.Size = new Size(33, 15);
            lbUnitPrice.TabIndex = 5;
            lbUnitPrice.Text = "Price";
            // 
            // lbQuantity
            // 
            lbQuantity.AutoSize = true;
            lbQuantity.Location = new Point(650, 59);
            lbQuantity.Name = "lbQuantity";
            lbQuantity.Size = new Size(53, 15);
            lbQuantity.TabIndex = 8;
            lbQuantity.Text = "Quantity";
            // 
            // dgvProduct
            // 
            dgvProduct.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvProduct.Location = new Point(400, 214);
            dgvProduct.Margin = new Padding(3, 2, 3, 2);
            dgvProduct.Name = "dgvProduct";
            dgvProduct.RowHeadersWidth = 51;
            dgvProduct.RowTemplate.Height = 29;
            dgvProduct.Size = new Size(584, 141);
            dgvProduct.TabIndex = 9;
            dgvProduct.DoubleClick += dgvProduct_DoubleClick;
            // 
            // dgvChoose
            // 
            dgvChoose.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvChoose.Location = new Point(27, 22);
            dgvChoose.Margin = new Padding(3, 2, 3, 2);
            dgvChoose.Name = "dgvChoose";
            dgvChoose.RowHeadersWidth = 51;
            dgvChoose.RowTemplate.Height = 29;
            dgvChoose.Size = new Size(351, 276);
            dgvChoose.TabIndex = 10;
            dgvChoose.CellContentClick += dataGridView2_CellContentClick;
            // 
            // txtQuantity
            // 
            txtQuantity.Location = new Point(731, 54);
            txtQuantity.Margin = new Padding(3, 2, 3, 2);
            txtQuantity.Name = "txtQuantity";
            txtQuantity.Size = new Size(110, 23);
            txtQuantity.TabIndex = 14;
            // 
            // txtProductName
            // 
            txtProductName.Location = new Point(506, 54);
            txtProductName.Margin = new Padding(3, 2, 3, 2);
            txtProductName.Name = "txtProductName";
            txtProductName.Size = new Size(110, 23);
            txtProductName.TabIndex = 15;
            // 
            // txtUnitPrice
            // 
            txtUnitPrice.Location = new Point(506, 109);
            txtUnitPrice.Margin = new Padding(3, 2, 3, 2);
            txtUnitPrice.Name = "txtUnitPrice";
            txtUnitPrice.Size = new Size(110, 23);
            txtUnitPrice.TabIndex = 16;
            // 
            // frmCustomer
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1011, 364);
            Controls.Add(txtUnitPrice);
            Controls.Add(txtProductName);
            Controls.Add(txtQuantity);
            Controls.Add(dgvChoose);
            Controls.Add(dgvProduct);
            Controls.Add(lbQuantity);
            Controls.Add(lbUnitPrice);
            Controls.Add(lbProductName);
            Controls.Add(btnClose);
            Controls.Add(btnBuy);
            Controls.Add(btnChoose);
            Controls.Add(btnAccount);
            Margin = new Padding(3, 2, 3, 2);
            Name = "frmCustomer";
            Text = "frmCustomer";
            Load += frmCustomer_Load;
            ((System.ComponentModel.ISupportInitialize)dgvProduct).EndInit();
            ((System.ComponentModel.ISupportInitialize)dgvChoose).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnAccount;
        private Button btnChoose;
        private Button btnBuy;
        private Button btnClose;
        private Label lbProductName;
        private Label lbUnitPrice;
        private Label label3;
        private Label label4;
        private Label lbQuantity;
        private DataGridView dgvProduct;
        private DataGridView dgvChoose;
        private TextBox textBox1;
        private TextBox textBox2;
        private TextBox textBox3;
        private TextBox txtQuantity;
        private TextBox textBox5;
        private TextBox txtProductName;
        private TextBox txtUnitPrice;
    }
}