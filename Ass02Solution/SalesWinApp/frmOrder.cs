﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessObject.BusinessObject;
using DataAccess;
using DataAccess.Repository;

namespace SalesWinApp
{
    public partial class frmOrder : Form
    {
        public frmOrder()
        {
            InitializeComponent();
        }
        public Order order { get; set; }
        private void frmOrder_Load(object sender, EventArgs e)
        {
            if (order != null)
            {
                DateTime orderDate, requiredDate, shippedDate;
                txtOrderID.Text = order.OrderId.ToString();
                txtMemberID.Text = order.MemberId.ToString();
                txtOrderDate.Text = order.OrderDate.ToString();
                if (order.RequiredDate.ToString().Length != 0)
                {
                    txtRequiredDate.Text = order.RequiredDate.ToString();
                }
                else
                {
                    txtRequiredDate.Text = string.Empty;
                }
                txtShippedDate.Text = order.ShippedDate.ToString();
                if (order.Freight != null)
                {
                    txtFreight.Text = order.Freight.ToString();
                }
                else { txtFreight.Text = string.Empty; }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                Order o = new Order
                {
                    OrderId = int.Parse(txtOrderID.Text),
                    MemberId = int.Parse(txtMemberID.Text),
                    OrderDate = DateTime.Parse(txtOrderDate.Text),
                    RequiredDate = DateTime.Parse(txtRequiredDate.Text),
                    ShippedDate = DateTime.Parse(txtShippedDate.Text),
                    Freight = decimal.Parse(txtFreight.Text),
                };
                IOrderRepository orderRepository = new OrderRepository();
                orderRepository.UpdateOrder(o);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Update Order");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
