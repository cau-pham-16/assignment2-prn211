﻿using BusinessObject.BusinessObject;
using DataAccess.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SalesWinApp
{
    public partial class frmProduct : Form
    {
        public frmProduct()
        {
            InitializeComponent();
        }
        public bool InsertOrUpdate { get; set; }
        public Product product { get; set; }
        public IProductRepository productRepository;
        private void frmProduct_Load(object sender, EventArgs e)
        {
            txtProductID.Enabled = !InsertOrUpdate;
            if (InsertOrUpdate == true)
            {
                txtProductID.Text = product.ProductId.ToString();
                txtCategoryID.Text = product.CategoryId.ToString();
                txtProductName.Text = product.ProductName;
                txtUnitPrice.Text = product.UnitPrice.ToString();
                txtUnitSln.Text = product.UnitslnStock.ToString();
                txtWeight.Text = product.Weight;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Product p = new Product
                {
                    ProductId = int.Parse(txtProductID.Text),
                    CategoryId = int.Parse(txtCategoryID.Text),
                    ProductName = txtProductName.Text,
                    Weight = txtWeight.Text,
                    UnitPrice = decimal.Parse(txtUnitPrice.Text),
                    UnitslnStock = int.Parse(txtUnitSln.Text),
                };
                if (InsertOrUpdate == true)
                {
                    productRepository.UpdateProduct(p);
                }
                else
                {
                    productRepository.InsertProduct(p);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, InsertOrUpdate == true ? "UpdateProduct" : "Add Product");
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
